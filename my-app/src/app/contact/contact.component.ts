import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.css'
})
export class ContactComponent {
  teamMembers = [
    { name: 'Dr. Ananya Sharma', role: 'Founder & CEO', email: 'amit.kumar@example.com', phone: '+91 1234567890', bio: 'Amit Kumar is the visionary behind our organization. He leads the strategic direction and ensures that our mission is fulfilled.' },
    { name: 'Priya Patel', role: 'Marketing Manager', email: 'priya.patel@example.com', phone: '+91 9876543210', bio: 'Priya Patel handles all marketing initiatives and public relations activities. She is passionate about spreading awareness for our cause.' },
    { name: 'Rahul Sharma', role: 'Fundraising Coordinator', email: 'rahul.sharma@example.com', phone: '+91 8765432109', bio: 'Rahul Sharma is responsible for organizing fundraising events and campaigns. He ensures that our financial goals are met.' },
    { name: 'Anjali Singh', role: 'Volunteer Coordinator', email: 'anjali.singh@example.com', phone: '+91 7654321098', bio: 'Anjali Singh recruits and manages volunteers for various projects and initiatives. She ensures smooth coordination and communication.' },
    { name: 'Neha Gupta', role: 'Finance Manager', email: 'neha.gupta@example.com', phone: '+91 6543210987', bio: 'Neha Gupta oversees the financial aspects of our organization, including budgeting, accounting, and financial reporting.' }
  ];

  constructor() { }

}
