import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrl: './about.component.css'
})
export class AboutComponent {
  mission = 'Our mission at Hope for India is to provide education and healthcare access to underprivileged communities across India. We believe in the power of education and healthcare to transform lives and empower individuals to break the cycle of poverty.';
  story = 'Hope for India was founded in 2010 by Dr. Ananya Sharma, a dedicated physician who witnessed firsthand the lack of healthcare and educational opportunities in rural India. Since its inception, Hope for India has established multiple clinics and schools, impacting over 10,000 lives.';
  team = 'Our team is comprised of passionate professionals from diverse backgrounds, including doctors, educators, and social workers. Together, we work tirelessly to ensure that every child has access to quality education and every family receives essential healthcare services.';
  impact = 'Over the past decade, Hope for India has made significant strides in improving healthcare and education outcomes in rural communities. We have reduced infant mortality rates by 30% and increased school enrollment by 50%. We are committed to continuing our efforts to create lasting change.';
  joinUs = 'We invite you to join us in our mission to build a brighter future for the children of India. Whether you choose to volunteer your time, donate to support our programs, or spread awareness about our cause, your contribution makes a difference. Together, we can make hope a reality for every child.';

}
