import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrl: './events.component.css'
})
export class EventsComponent {
  events = [
    { 
      id: 1, 
      name: 'Charity Gala Dinner', 
      description: 'An elegant evening of fine dining and entertainment in support of our cause.', 
      date: 'Saturday, 15th May 2024', 
      venue: 'Grand Ballroom, Taj Mahal Palace, Mumbai', 
      image: 'Designer (8).png'
    },
    { 
      id: 2, 
      name: 'Marathon for Education', 
      description: 'Join us for a marathon to raise funds for education initiatives in rural areas.', 
      date: 'Sunday, 23rd June 2024', 
      venue: 'Starting point: Gateway of India, Mumbai',
      image: 'Designer (9).png'
    },
    { 
      id: 3, 
      name: 'Art Auction for Health', 
      description: 'A charity art auction featuring works by renowned artists to support healthcare projects.', 
      date: 'Friday, 11th October 2024', 
      venue: 'Art Gallery, National Museum, New Delhi',
      image: 'Designer (7).png'
    },
    { 
      id: 4, 
      name: 'Food Drive for Hunger Relief', 
      description: 'Collecting non-perishable food items to support underprivileged families in rural areas.', 
      date: 'Saturday, 2nd November 2024', 
      venue: 'Collection Centers across India',
      image: 'Designer (6).png' 
    },
    { 
      id: 5, 
      name: 'Concert for Climate Action', 
      description: 'An outdoor concert featuring live performances by popular artists to raise awareness about climate change.', 
      date: 'Sunday, 15th December 2024', 
      venue: 'Venue: Eco Park, Kolkata',
      image: 'Designer (5).png'
    },
    { 
      id: 6, 
      name: 'Fashion Show for Empowerment', 
      description: 'A fashion show showcasing designs by emerging designers to empower women in rural communities.', 
      date: 'Saturday, 18th January 2025', 
      venue: 'Event Space, DLF Mall of India, Noida',
      image: 'Designer (10).png'
    },
    { 
      id: 7, 
      name: 'Cycling Challenge for Clean Water', 
      description: 'A cycling challenge to raise funds for clean water projects in drought-affected areas.', 
      date: 'Sunday, 2nd March 2025', 
      venue: 'Starting point: India Gate, New Delhi',
      image: 'Designer (11).png' 
    },
    { 
      id: 8, 
      name: 'Virtual Yoga Marathon for Mental Health', 
      description: 'A virtual yoga marathon to promote mental well-being and raise funds for mental health programs.', 
      date: 'Saturday, 10th May 2025', 
      venue: 'Online Event',
      image: 'Designer (8).png'
    },
    // { 
    //   id: 9, 
    //   name: 'Tech Hackathon for Social Innovation', 
    //   description: 'A hackathon bringing together tech enthusiasts to develop innovative solutions for social challenges.', 
    //   date: 'Saturday, 20th July 2025', 
    //   venue: 'Tech Hub, Bangalore' 
    // },
    // { 
    //   id: 10, 
    //   name: 'Film Screening for Diversity', 
    //   description: 'A film screening event showcasing movies that celebrate diversity and inclusivity.', 
    //   date: 'Sunday, 8th September 2025', 
    //   venue: 'Cinema Hall, PVR Plaza, Connaught Place, New Delhi' 
    // }
  ];

  constructor(private router: Router) {}

  navigateToCreateEvent() {
    this.router.navigate(['/create-event']);
  }

  navigateToEvent(eventId: number) {
    this.router.navigate(['/event', eventId]);
  }
}
